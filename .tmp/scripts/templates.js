define(function(){

this["JST"] = this["JST"] || {};

this["JST"]["app/scripts/templates/graph.ejs"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="graph">\n    \n    <input type="text" value="" name="graphTitle" placeholder="Set your own title"></input>\n    <select class="graph-types"></select>\n    <div class="container">\n\n    </div>\n</div>';

}
return __p
};

this["JST"]["app/scripts/templates/instructions.ejs"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div>\n    <h1>datapine\'s Senior Frontend Developers\' test</h1>\n    <p>We would like you to build a small charting application, using this template or something totally different, it is up to you. We would prefer it if you use the below libraries, but again, if you know something similar, and believe you can do a better job with it then by all means use that instead. However, your application must be a SPA. We would prefer your application to not look like a typical bootstrap application...</p>\n    <p>We would like you to use something such as the following:</p>\n    <ul>\n      <li>Backbone+Backbone Layoutmanager / Angular.js</li>\n      <li>RequireJS</li>\n      <li>Highcharts</li>\n      <li>CSS written with LESS/SASS and not plain CSS like this template currently has. </li>\n    </ul>\n    <p>We would like your application to load a number of charts (4+ depending upon your thumbnail size and application design) on a page. These charts should adhere to these conditions:</p>\n    <ul>\n        <li>All charts should be in thumbnail mode on the initial page</li>\n        <li>All charts should be given a full view mode (lightbox or new page). If you can do this via routes, that would be awesome.</li>\n        <li>All charts should be able to switch something simple, such as the chart type or chart name. <em>Persistent data isn\'t expected to be implemented if you\'re using JSON files</em></li>\n        <li>The chart data should be pulled from a resource. A simple .json file would suffice</li>\n    </ul>\n    <p>Additional to the charts, we would like you to make a menu that allows you switch between the initial page and a page that explains (in brief) your test and how you go about loading it &amp;c. <em>(typical README.md stuff)</em></p>\n    <p>We will be judging your test on how your application looks and feels, meeting the above specifications, the quality of code and how it has been executed. Anything additional that you may add to show off your best skills <em>(such as a chart filter)</em> is always welcome.</p>\n    <p>Once your test is complete, we would ideally prefer it if you uploaded your test to a public repo and sent us the details. You could alternatively send us the file via email</p>\n  </div>';

}
return __p
};

this["JST"]["app/scripts/templates/readme.ejs"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="readme">\n    <article class="markdown-body entry-content" itemprop="text"><h2>README</h2>\n\n<p>I started investigating for design patterns in backbonejs, it was a while since I made my last application in Backbone, and wanted to have a fresh look of any changes of the framework.</p>\n\n<p>I did the same thing for LayoutManager, and Highcharts, with the exception that I didn\'t knew anything about it, and it was quite hard compared with backbone, and the project samples in the project documentation were the most helpful ones. For highcharts was the tutorialize page.</p>\n\n<p>Then I started to work in the Gruntfile, I found it very organized, so it was straight forward to add the LESS tasks (less, concat and copy).</p>\n\n<p>Then I added cleaned up the project, and created a basic structure, the project isn\'t that big so I try to keep it simple but not simplistic, I created one file per subject (controllers, views, models, router), to test the loading order I added console.log where was required just to see if everything was alright.</p>\n\n<p>Then I started working in the router and the controller, I had sketched a simple Layout consisting of three pages (main, instructions, graph details) and the views in them. Later on, I realized I needed another for the README.</p>\n\n<p>With the routes created then I worked in the views, they consist of templates, it took me a while to understand how they were being loaded and added to the project, once I understood all the JST, global spacing and that was part of the requirejs, I implemented a single loader in the controller to load and fill the views into the main layout.</p>\n\n<p>Tested again, make sure all the views were being loaded with the change of url in the browser. With that working, I started to work in the HTML and the styles. Created a basic navigation bar, a menu button, and the menu, I though will be nice to have some icons so I added font-awesome to bower and added to the project.</p>\n\n<p>Then I work in the load of the charts, I thought it will be better to load a JSON with a list of charts, this list will be loaded in a collection where I could grab individual charts properties to show the thumbnails and the urls for the loading of the full-page graphs. While doing this I realized it will be good to have a config module with basic parameters for the application, and I created the resources folder for the JSON files.</p>\n\n<p>With that done I loaded the thumbnails that I got from the highcharts page, I did the LESS to adjust the thumbnails. and I start working in the full page highcharts, this was more straight forward than I thought, I guess it was because the charting library is very easy to use at least for simple plotting, just send a JSON with the options, and series, and call .hightcharts there is a graph. Then I started to work in changing options for the graph (title and type).</p>\n\n<p>That was it, some things toke me more time than expected particularly understand the whole Layout manager configuration and loading. but was able to solve it Also I did structural changes on the go somethings that I found out of place I move them for a better one, for example: there was some logic for the views setting that was in the router that I moved to the controller, many logic for the load of the models that I put in the view that logically fit better in the collections. And so on.</p>\n\n<h1>There are many things that can be improved:</h1>\n\n<ul>\n<li><p>The routing can be implemented to load from a resource instead, of this manual steps of creating a simple view, create the handler in the controller and then the route. </p></li>\n<li><p>The styles can be better and some updates to make it responsive.</p></li>\n<li><p>I think I polluted the grunt task, that could be done better (the less and the concat come to mind)</p></li>\n</ul>\n\n<p>There are no changes for the project execution:</p>\n\n<pre><code>$ bower install &amp;&amp; npm install\n$ grunt server\n</code></pre>\n\n</article>\n  </div>';

}
return __p
};

this["JST"]["app/scripts/templates/thumbnail.ejs"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="thumbnail">\n    <h3>' +
((__t = ( data.name )) == null ? '' : __t) +
'</h3>\n    <div class="container">\n        <a href="#/graph/' +
((__t = ( data.id)) == null ? '' : __t) +
'">\n            <img src="img/' +
((__t = ( data.id)) == null ? '' : __t) +
'.png" alt="' +
((__t = ( data.name)) == null ? '' : __t) +
'">\n        </a>\n    </div>\n</div>';

}
return __p
};

this["JST"]["app/scripts/templates/welcome.ejs"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<h1 class="welcome-message">Welcome! Click an image to open the detailed graph.</h2>';

}
return __p
};

  return this["JST"];

});