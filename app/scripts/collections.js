// set the collections models
'use strict';

define(['backbone', 'models', 'config'], function(Backbone, models, config) {
    // set the DataChartCollection with DataChart objects
    // methods
    // byId: filter models by Id
    // getType: return the unique chart types from the ones that were loaded
    var DataChartCollection = Backbone.Collection.extend({
        model: models.DataChart,
        byId: function(id) {
            var dataChart = this.filter(function(dchart) {
                return dchart.get('id') === id;
            });
            return dataChart;
        },
        getTypes: function() {
            return _.uniq(this.models, function(item, key) {
                return item.attributes.type;
            }).map(function(item) {
                return item.attributes.type;
            });
        }
    });


    var dataChartList = new DataChartCollection();
    // init the collection with the charts JSON object from the config
    // and fill the dataChartList
    // return the $.ajax promise
    var init = function init(callback) {
        var promise = $.ajax({
            url: config.charts_path,
            dataType: 'json'
        }).success(function(result) {
            dataChartList = new DataChartCollection(result);
        });

        return promise;
    };

    return {
        initialize: init,
        getChartList: function() {
            return dataChartList;
        }
    };
});
