// set the routes rules for the application
// return initialize function to set the layout and routes.
'use strict';

define(['backbone', 'layoutmanager'], function(Backbone, manager, views) {
    // routes definition
    var AppRouter = Backbone.Router.extend({
        routes: {
            'graph/:id': 'loadGraph',
            'instructions': 'loadInstructions',
            'readme': 'loadReadme',
            '*actions': 'loadDefault'
        }
    });

    var router = new AppRouter;

    // bind the router actions with the controller methods
    var setRoutes = function setRoutes(controller) {
        
        router.on('route:loadGraph', controller.loadGraph);

        router.on('route:loadInstructions', controller.loadInstructions);

        router.on('route:loadDefault', controller.loadDefault);

        router.on('route:loadReadme', controller.loadReadme);

        // is needed for the bookmarking of urls
        Backbone.history.start();
    }

    // initialize the application and wait for the controller to load the routes
    var init = function(controller) {
        controller.initialize().done(function() {
            setRoutes(controller);
        });
    };

    return { initialize: init };
});
