//mange the interactions between models, collections and views
'use strict';

define(['backbone', 'collections', 'views', 'layoutmanager'], function(Backbone, collections, views, manager) {
    var main;

    // build the basic layout for the page
    function buildLayout() {
        Backbone.Layout.configure({
            manage: true
        });

        main = new Backbone.Layout({
            manage: true,
            template: "#main-layout",
        });

        main.$el.appendTo(".page-container");
        main.render();
    };

    // add/append views into the main layout
    // viewName -> the name of the view from the views object
    // insert -> (true: append the view, false: replace the previous view)
    // parameters -> object to set the model of the view
    var applyView = function applyView(viewName, insert, parameters) {
        parameters = parameters || {};
        main.setView(".content", new views[viewName](parameters), insert);
        main.render();
    };

    // setting of basic views
    var loadInstructions = function loadInstructions() {
        applyView('instructions');
    };

    var loadReadme = function loadInstructions() {
        applyView('readme');
    };

    // load the default view
    // iterates over the charList collection and set the correspondant views
    var loadDefault = function loadDefault() {
        applyView('welcome');
        var chartList = collections.getChartList();
        _.each(chartList.models, function(dchart) {
            applyView('thumbnail', true, { model: dchart });
        });
    };

    // set the graph view with the id from the route
    var loadGraph = function loadGraph(id) {
        var chart,
            chartList = collections.getChartList();

        if (chartList) {
            chart = chartList.byId(id);
        }

        // get the JSON chart object from the URL in the DataChart
        // and apply the view corresponding view
        if (chart && chart.length > 0) {
            $.ajax({
                url: chart[0].get('url'),
                dataType: 'json'
            }).success(function(result) {
                if (result) {
                    var hChart = new Backbone.Model(result);
                    applyView('graph', false, { model: hChart, types: chartList.getTypes() });
                    console.log('graph id:', id);
                }
            });
        }
    };

    var init = function init() {
        buildLayout();
        return collections.initialize();
    };

    return {
        initialize: init,
        loadGraph: loadGraph,
        loadDefault: loadDefault,
        loadInstructions: loadInstructions,
        loadReadme: loadReadme
    };
});
