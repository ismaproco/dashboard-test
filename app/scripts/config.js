'use strict';
// load basic configuration for app
define([], function() {
    return {
        charts_path: 'scripts/resources/charts.json' // load JSON with the chart properties
    };
});
