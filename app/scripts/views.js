// Define all the views of the application
'use strict';

define(['backbone', 'layoutmanager', 'templates', 'highcharts'], function(Backbone, manager, templates, charts) {
    // Static views
    // just load the template for each view
    var instructionsView = Backbone.View.extend({
        template: templates['app/scripts/templates/instructions.ejs']
    });

    var welcomeView = Backbone.View.extend({
        template: templates['app/scripts/templates/welcome.ejs']
    });

    var readmeView = Backbone.View.extend({
        template: templates['app/scripts/templates/readme.ejs']
    });

    // view for the thumbnails in the main page
    var thumbnailView = Backbone.View.extend({
        //Set the data property with the model attributes to be used in the template
        serialize: function() {
          return { data: this.model.attributes };
        },
        afterRender: function() {
          //remove the parent div for problems of styling
          this.$el.find('.thumbnail').unwrap();
        },
        template: templates['app/scripts/templates/thumbnail.ejs']
    });

    // Graph view
    var graphView = Backbone.View.extend({
        template: templates['app/scripts/templates/graph.ejs'],
        afterRender: function() {
            var self = this;
            //cached the original title of the chart
            var originalTitle = self.model.attributes.title.text;

            var $container = self.$el.find('.container');
            $container.highcharts(self.model.attributes);

            // when the input text in the template chages, set the title of the 
            // graph with the new value, if the value is blank reset the title
            self.$el.find('input[name="graphTitle"]').change(function() {
                var text = $(this).val();

                if (text.length > 0) {
                    self.model.attributes.title.text = text;
                    $container.highcharts(self.model.attributes);
                } else {
                    self.model.attributes.title.text = originalTitle;
                }
                // reset highcharts
                $container.highcharts(self.model.attributes);
            });

            // build the select with the chart types
            var $select = self.$el.find('.graph-types');

            self.types.forEach(function(type) {
                $select.append($('<option/>').html(type));
            });

            //reload the chart with the selected chart type in the select
            $select.change(function() {
                self.model.attributes.chart.type = $(this).find(':selected').text();
                $container.highcharts(self.model.attributes);
            });
        }
    });

    //global actions
    (function(){
          $('.btn-menu').click(function() {
          $(this).toggleClass('push');
          $('.menu').toggleClass('push');
        });
    })();

    return {
        instructions: instructionsView,
        welcome: welcomeView,
        thumbnail: thumbnailView,
        graph: graphView,
        readme: readmeView
    }
});
