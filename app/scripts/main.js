'use strict';
// Config the application and initialize the routing
require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        }
    },
    paths: {
        jquery: '../bower_components/jquery/jquery',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/underscore/underscore',
        layoutmanager: '../bower_components/layoutmanager/backbone.layoutmanager',
        highcharts: '../bower_components/highcharts-release/highcharts'
    }
});

require([
    'backbone',
    'controllers',
    'router'
], function (Backbone, controller, router) {
    router.initialize( controller );
});
